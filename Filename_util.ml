open Unix

let prng = lazy(Random.State.make_self_init ());;

let tmp_dir_name tmp_dir prefix suffix =
  let rnd = (Random.State.bits (Lazy.force prng)) land 0xFFFFFF in
  Filename.concat tmp_dir (Printf.sprintf "%s%06x%s" prefix rnd suffix)

let create_tmp_dir tmp_dir : string =
  let prefix = "bandcamp-" in
  let suffix = "-tmp-zip-extract" in
  let rec try_name counter =
    let name = tmp_dir_name tmp_dir prefix suffix in
    try
      closedir (opendir name);
      name
    with
    | Unix_error(ENOENT, _, _) ->
       mkdir name 0o755;
       name
    | e ->
       if counter >= 1000 then raise e else try_name (counter + 1)
  in try_name 0
