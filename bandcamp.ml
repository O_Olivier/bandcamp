(*
 * For each zip file in the ~/Downloads directory :
 *   Extract it temporarily
 *   if the extracted files are not multimedia
 *   then
 *     Discard them
 *   else
 *     Retrieve the artist and album names from the temporary extracted files
 *     Create (if needed) corresponding subdirectories
 *     Copy the files from the temporary location to the right directory
 *     Delete the zip file
 *)

open Unix

(* -------------------------------------------------------------------------- *)

(* Global variables *)

let home = getenv "HOME"

let download_dir = Filename.concat home "Téléchargements"

let music_dir = Filename.concat home "Musique"

let tmp_dir = "/tmp"

let zip_regexp = Str.regexp ".+\\.zip"

(* -------------------------------------------------------------------------- *)

(* Main function, operates on each zip files. *)
let foreach_zipfile (f:string -> unit) : unit =
  let d = opendir download_dir in
  let rec read () =
    try
      let filename = readdir d in
      if Str.string_match zip_regexp filename 0 then
        f filename;
      read ()
    with End_of_file ->
      ()
  in
  read ();
  closedir d

(* Call the unzip command. *)
let unzip zipfile dest : Unix.process_status =
  let src = Filename.concat download_dir zipfile in
  let cmd = Filename.quote_command "unzip" [
    "-q" ; (* quiet *)
    src  ;
    "-d" ;
    dest ;
  ] in
  system cmd

(* Call the cp command. *)
let cp src dest : Unix.process_status =
  let cmd = Filename.quote_command "cp" [
    "-r" ;
    src  ;
    dest ;
  ] in
  system cmd

(* Extract the zip file into a temporary location. *)
let temporary_extract zipfile : string * Unix.process_status =
  let dirname = Filename_util.create_tmp_dir tmp_dir in
  let status = unzip zipfile dirname in
  dirname, status

type infos = { artist: string; album: string }

(* Ensure we are dealing with at least one music file.
   Dig into metadata to find infos. *)
let get_infos filename : infos option =
  let open Taglib in
  try
    let file = File.open_file `Autodetect filename in
    let artist = tag_artist file in
    let album = tag_album file in
    File.close_file file;
    Some { artist; album }
  with
  | File.Not_implemented ->
     None
  | File.Invalid_file ->
     None

(* Search for a music file. Retrieve the artist and album names and return them
   along with the path to the directory where are located the music file. *)
let retrieve_names (dname:string) : string * (infos option) =

  (* Depth first search *)
  let rec search prefix cur_dir_handle =
    try
      let filename = readdir cur_dir_handle in
      let complete_filename = Filename.concat prefix filename in

      (* If the entry is '.' or '..' then we skip to the next entry. *)
      if (String.equal filename Filename.current_dir_name
          || String.equal filename Filename.parent_dir_name)
      then
        search prefix cur_dir_handle

      (* If the entry is a directory we call ourself recursively. *)
      else if Sys.is_directory complete_filename then
        let d = opendir complete_filename in
        let res = search complete_filename d in
        closedir d;
        res

      (* If the entry is a file, we try to read its metadata *)
      else
        match get_infos complete_filename with
        | None ->
           search prefix cur_dir_handle
        | infos ->
           prefix, infos
    with End_of_file ->
      prefix, None
  in
  let new_dir_handle = opendir dname in
  let res = search dname new_dir_handle in
  closedir new_dir_handle;
  res

(* Create artist and album directory if needed. Return the album directory. *)
let create_directories artist album =
  let artist_dir = Filename.concat music_dir artist in
  if not @@ Sys.file_exists artist_dir then
    Unix.mkdir artist_dir 0o755;
  let album_dir = Filename.concat artist_dir album in
  if not @@ Sys.file_exists album_dir then
    Unix.mkdir album_dir 0o755;
  album_dir

let delete_zipfile zipfile =
  Sys.remove  (Filename.concat download_dir zipfile)

exception SomethingWentWrong

let log status =
  match status with
  | WEXITED 0 ->
     ()
  | _ ->
     raise SomethingWentWrong

let on_file zipfile : unit =
  Printf.printf "Trying to extract %s\n" zipfile;
  let extract_dir, status = temporary_extract zipfile in
  log status;
  let extract_album_dir, infos_opt = retrieve_names extract_dir in
  match infos_opt with
  | None ->
     Printf.printf "Directory %s doesn't contain multimedia files.\n\n" zipfile

  | Some { artist; album} ->
     let album_dir = create_directories artist album in
     let status = cp (Filename.concat extract_album_dir ".") album_dir in
     log status;
     Printf.printf "%s was extracted correctly\n\n" zipfile;
     delete_zipfile zipfile

let () =
  foreach_zipfile on_file
